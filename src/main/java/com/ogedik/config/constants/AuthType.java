package com.ogedik.config.constants;

/**
 * @author orkun.gedik
 */
public enum AuthType {

    NO_AUTH,

    BASIC_AUTH,
}
