/**
 * © 2020 Copyright Amadeus Unauthorised use and disclosure strictly forbidden.
 */
package com.ogedik.config.model;

/**
 * @author orkun.gedik
 */
public class JiraLoginInfo {
  private String failedLoginCount;
  private String loginCount;
  private String lastFailedLoginTime;
  private String previousLoginTime;
}
